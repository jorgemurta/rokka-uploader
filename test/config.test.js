const config = require('../src/config')

test('config schema', () => {
  const schema = config.schema

  expect(schema).toHaveProperty('isJoi', true)
})

test('config validation passes with minimal valid schema', () => {
  const schema = {
    ROKKA_API_KEY: '11112222333344445555666677778888',
    ROKKA_ORGANISATION: 'test'
  }

  const result = config.validate(schema)

  expect(result.ROKKA_API_KEY).toEqual(schema.ROKKA_API_KEY)
  expect(result.ROKKA_ORGANISATION).toEqual(schema.ROKKA_ORGANISATION)
})

test('config validation discards irrelevant variables', () => {
  const schema = {
    COLORTERM: 'yes',
    COMMAND_MODE: 'unix2003',
    ROKKA_API_KEY: '11112222333344445555666677778888',
    ROKKA_ORGANISATION: 'test',
    TERM: 'xterm-256color'
  }

  const result = config.validate(schema)

  expect(result.COLORTERM).toBeUndefined()
  expect(result.COMMAND_MODE).toBeUndefined()
  expect(result.ROKKA_ORGANISATION).toEqual(schema.ROKKA_ORGANISATION)
  expect(result.TERM).toBeUndefined()
})

test('config validation throws when required keys are invalid', () => {
  const schema = {
    ROKKA_API_KEY: 'fail',
    ROKKA_ORGANISATION: 'test'
  }

  function badValidation() {
    config.validate(schema)
  }

  expect(badValidation).toThrow(/child "ROKKA_API_KEY" fails/)
})

test('config validation throws when required keys are missing', () => {
  const schema = {
    ROKKA_API_KEY: '11112222333344445555666677778888'
  }

  function badValidation() {
    config.validate(schema)
  }

  expect(badValidation).toThrow(/child "ROKKA_ORGANISATION" fails/)
})

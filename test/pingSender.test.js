const { patchMetaDataKeysInRokkaResponse } = require('../src/pingSender')

test('meta data is patched correctly', () => {
  const mockRokkaResponse = {
    user_metadata: {
      foo: 'bar'
    }
  }
  const mockConfig = {
    METADATA_STATIC_TAG_KEY: 'str:foo',
    METADATA_STATIC_TAG_VALUE: 'bar'
  }

  const patchedRokkaResponse = patchMetaDataKeysInRokkaResponse(
    mockConfig,
    mockRokkaResponse
  )

  expect(patchedRokkaResponse).toHaveProperty('user_metadata.str:foo', 'bar')
  expect(patchedRokkaResponse).not.toHaveProperty('user_metadata.foo')
})

test('meta data as array is patched correctly', () => {
  const mockRokkaResponse = {
    user_metadata: {
      foo: 'bar',
      moo: 'foo'
    }
  }
  const mockConfig = {
    METADATA_STATIC_TAG_KEY: ['str:foo', 'str:moo'],
    METADATA_STATIC_TAG_VALUE: ['bar', 'str']
  }

  const patchedRokkaResponse = patchMetaDataKeysInRokkaResponse(
    mockConfig,
    mockRokkaResponse
  )

  expect(patchedRokkaResponse).toHaveProperty('user_metadata.str:foo', 'bar')
  expect(patchedRokkaResponse).toHaveProperty('user_metadata.str:moo', 'foo')
  expect(patchedRokkaResponse).not.toHaveProperty('user_metadata.foo')
  expect(patchedRokkaResponse).not.toHaveProperty('user_metadata.moo')
})

test(`patch shouldn't fail when user_metadata is missing from rokka response`, () => {
  const mockRokkaResponse = {
    foo: 'bar'
  }
  const mockConfig = {
    METADATA_STATIC_TAG_KEY: 'str:foo',
    METADATA_STATIC_TAG_VALUE: 'bar'
  }

  const patchedRokkaResponse = patchMetaDataKeysInRokkaResponse(
    mockConfig,
    mockRokkaResponse
  )

  expect(patchedRokkaResponse).not.toHaveProperty('user_metadata')
})

test(`patch shouldn't fail when there is no config for static metadata`, () => {
  const mockRokkaResponse = {
    user_metadata: {
      foo: 'bar'
    }
  }
  const mockConfig = {}

  const patchedRokkaResponse = patchMetaDataKeysInRokkaResponse(
    mockConfig,
    mockRokkaResponse
  )

  expect(patchedRokkaResponse).toHaveProperty('user_metadata.foo')
})

test(`patch shouldn't do anything if rokka sends the correct key`, () => {
  const mockRokkaResponse = {
    user_metadata: {
      'str:foo': 'bar'
    }
  }
  const mockConfig = {
    METADATA_STATIC_TAG_KEY: 'str:foo',
    METADATA_STATIC_TAG_VALUE: 'bar'
  }

  const patchedRokkaResponse = patchMetaDataKeysInRokkaResponse(
    mockConfig,
    mockRokkaResponse
  )

  expect(patchedRokkaResponse).toHaveProperty('user_metadata.str:foo', 'bar')
  expect(patchedRokkaResponse).not.toHaveProperty('user_metadata.foo')
})

const crypto = require('crypto')
const fs = require('fs')

async function hashFileContents(filePath) {
  return new Promise((resolve, reject) => {
    const hash = crypto.createHash('sha1')

    const stream = fs.ReadStream(filePath)

    stream.on('data', function (data) {
      hash.update(data)
    })

    stream.on('end', function () {
      resolve(hash.digest('hex'))
    })
  })
}

async function getExistingImage(config, fileName, rokkaClient) {
  const fileHash = await hashFileContents(fileName)

  try {
    const result = await rokkaClient.sourceimages.getWithBinaryHash(
      config.ROKKA_ORGANISATION,
      fileHash
    )

    if (result.body && result.body.items && result.body.items[0]) {
      return result.body.items[0]
    }

    return false
  } catch (err) {
    // Rokka returns 404 if image does not exist.
    if (err.statusCode === 404) {
      return false
    } else {
      throw err
    }
  }
}

module.exports = {
  getExistingImage
}

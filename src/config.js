const Joi = require('joi')

const schema = Joi.object().keys({
  CONSOLE_LOG_LEVEL: Joi.string()
    .default('warning')
    .only([
      'emerg',
      'alert',
      'crit',
      'error',
      'warning',
      'notice',
      'info',
      'debug'
    ]),
  DEDUPLICATE_BEFORE_UPLOAD: Joi.boolean().default(false),
  DELETE_AFTER_SUCCESSFUL_UPLOAD: Joi.boolean().default(false),
  DELETE_DUPLICATES: Joi.boolean().default(false),
  METADATA_FILENAMES_KEY: Joi.string(),
  METADATA_STATIC_TAG_KEY: Joi.alternatives().try(Joi.string(), Joi.array()),
  METADATA_STATIC_TAG_VALUE: Joi.alternatives().try(Joi.string(), Joi.array()),
  WATCH_MODE: Joi.string().valid(['local', 'sftp']).default('local'),
  MONITOR_GLOB: Joi.string(),
  SFTP_INTERVAL: Joi.number().default(600),
  SFTP_HOST: Joi.string().default('localhost'),
  SFTP_USER: Joi.string().when('WATCH_MODE', {
    is: 'sftp',
    then: Joi.required()
  }),
  SFTP_PASSWORD: Joi.string(),
  SFTP_PORT: Joi.number().default(22),
  SFTP_PATH: Joi.string().when('WATCH_MODE', {
    is: 'sftp',
    then: Joi.required()
  }),
  SFTP_FILENAME_PATTERN: Joi.string().when('WATCH_MODE', {
    is: 'sftp',
    then: Joi.required()
  }),
  PING_URL: Joi.alternatives()
    .try(
      Joi.array().items(Joi.string().uri({ scheme: ['http', 'https'] })),
      Joi.string().uri({ scheme: ['http', 'https'] })
    )
    .default([]),
  PING_RETRY_COUNT: Joi.number().integer().positive().default(3),
  PING_RETRY_WAIT_TIME: Joi.number().integer().positive().default(600),
  PWD: Joi.string(),
  ROKKA_API_KEY: Joi.string().alphanum().length(32).required(),
  ROKKA_ORGANISATION: Joi.string().required()
})

function validate(config) {
  const result = Joi.validate(config, schema, { stripUnknown: true })

  if (result.error) {
    console.error('Failed validating environmental configuration', result.value)
    throw result.error
  }

  return result.value
}

module.exports = {
  schema,
  validate
}
